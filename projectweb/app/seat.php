<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class seat extends Model
{
    protected $fillable = ['cus_id'];
    protected $primaryKey = 'seat_id';
}
