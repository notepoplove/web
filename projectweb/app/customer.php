<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    protected $fillable = ['cus_name','cus_username','cus_password','cus_phone','cus_birthday','cus_address'];
    protected $primaryKey = 'cus_id';
}
