<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\seat;
use App\history;
use App\comment;
use App\customer;
use App\store;
use DB;
use Session;
class SeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // echo "index";
      $store = store::all()->toArray();
      $seat = seat::all()->toArray();
      $history = history::all()->where('cus_id','=',Session::get('user_id'))->toArray();
      $comment = DB::table('customers')
      ->join("comments",'customers.cus_id','=','comments.cus_id')
      ->where('comments.cus_id',"=",Session::get('user_id'))
      ->get()->toArray();
      // dd($store);
      $customer = customer::all()->where('cus_id','=',Session::get('user_id'))->toArray();
      // dd($customer);
      return view('homedirectory.home',compact('seat','history','comment','customer','store'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "create";
        // return view('userdirectory.user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return view('homedirectory.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // echo $id;
        // $book = seat::find($id);
        $book = seat::where('seat_id', $id)->first()->toArray();
        // dd($book);
        return view('homedirectory.book',compact('book','seat_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo "update : ".$id;
        $cancel = $request->get("cancel");
        // echo "<br>";
        // echo "ID User ".Session::get('user_id');
        $book = seat::where('seat_id', $id)->first();

        // dd($book);
        $cus_id = 0;
        if($cancel == null){
          $cus_id = Session::get('user_id');
          $book->cus_id = $cus_id;
        }
        else {
          // echo "".$cancel;
          $cus_id = $cancel;
          $book->cus_id = $cus_id;
        }
        $book->save();


        $history = new history([
          'cus_id' => $cus_id,
          'seat_id' => $id
        ]);
        $history->save();

        return redirect()->route('booking.index');
        // echo "update";
        // dd($request);
        // // $nerd = seat::find($id);
        // // $nerd->name       = Input::get('name');
        // // $nerd->save();
        // return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "destroy";
    }

}
