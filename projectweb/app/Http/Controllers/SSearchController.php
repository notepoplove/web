<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer;
class SSearchController extends Controller
{
    public function searchCustomer($message){
      // echo "".$message;
      $customer = customer::where('cus_name',$message)
      ->orWhere('cus_name', 'like', '%' . $message . '%')->get()->toArray();
      // dd($customer);
      return view('staffdirectory.sbook',compact('customer'));
    }
}
