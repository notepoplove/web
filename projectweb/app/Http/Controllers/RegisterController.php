<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('userdirectory.user');
      echo "index";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('userdirectory.user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $month = $request->get('month');
      $dateNumber = '';
      if($month == 'January'){
        $dateNumber = '01';
      }
      else if($month == 'February'){
        $dateNumber = '02';
      }
      else if($month == 'March'){
        $dateNumber = '03';
      }
      else if($month == 'April'){
        $dateNumber = '04';
      }
      else if($month == 'May'){
        $dateNumber = '05';
      }
      else if($month == 'June'){
        $dateNumber = '06';
      }
      else if($month == 'July'){
        $dateNumber = '07';
      }
      else if($month == 'August'){
        $dateNumber = '08';
      }
      else if($month == 'September'){
        $dateNumber = '09';
      }
      else if($month == 'October'){
        $dateNumber = '10';
      }
      else if($month == 'November'){
        $dateNumber = '11';
      }
      else if($month == 'December'){
        $dateNumber = '12';
      }
      $date = $request->get('year')."-".$dateNumber."-".$request->get('day');
      $user = new customer([
        'cus_name' => $request->get('name'),
        'cus_username' => $request->get('email'),
        'cus_password' => $request->get('psw'),
        'cus_phone' => $request->get('phone'),
        'cus_birthday' => $date,
        'cus_address' => $request->get('address')
      ]);
      $user->save();
      return redirect()->route('userregister.create')->with('success','successfully');
        // return url('userregister');
      // return redirect('userdirectory');
        // route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "show :".$id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // echo "".$id;
      // $ID = $request->get('');
        // dd($request->get('name'));
        $customer = customer::where('cus_id', $id)->first();
        // dd($customer->toArray());
        $customer->cus_name = $request->get('name');
        $customer->cus_password = $request->get('npsw');
        $customer->cus_phone = $request->get('phone');
        $customer->cus_address = $request->get('address');
        $customer->save();
        return redirect()->route('booking.index')->with('success','successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }
}
