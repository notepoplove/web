<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use App\customer;
use DB;
use Session;

class UserController extends Controller
{
  function showUser(){
    return view('userdirectory.user');
  }
  function showSLogin(){
    return view('staffdirectory.slogin');
  }

  // function checkLogin(){
  //   return view('home');
  // }
  function checkLogin(Request $req){
      $username = $req->get('username');
      $password = $req->get('password');
      // echo $username;

      $checkLogin = DB::table('customers')->where(['cus_username'=>$username,'cus_password'=>$password])->get();
      // dd($checkLogin->toArray());
      if(count($checkLogin)>0)
      {
        $iduser;
        foreach($checkLogin as $id){
            $iduser = $id->cus_id;
        }
        Session::put('user_id', $iduser);
        // echo "".Session::get('user_id');
        // dd(Session::put('user_id', $iduser));

        return redirect()->route('booking.index');
      }
      else
      {
        return redirect()->back();
      }
  }
  function scheckLogin(Request $req){
    $username = $req->get('username');
    $password = $req->get('password');
    // dd($req->toArray());
    // echo $username;

    $checkLogin = DB::table('staff')->where(['staff_username'=>$username,'staff_password'=>$password])->get()->toArray();
    // dd($checkLogin);

    if(count($checkLogin)>0)
    {

      $idstaff = $checkLogin[0]->staff_id;
      Session::put('staff_id', $idstaff);
      // echo "".Session::get('user_id');
      // return view('homedirectory.home');
      return redirect()->route('sbooking.index');
    }
    else
    {
      return redirect()->back();
    }
  }
  function checkLogout(){
    return view('userdirectory.user');
  }
  function deleteUser($id){
    // echo "ID:".$id;
    DB::table('customers')->where('cus_id', '=', $id)->delete();
    return redirect()->route('sbooking.index');
  }
}
