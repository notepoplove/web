<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\seat;
use App\customer;
use DB;

class SHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $seat = seat::all()->toArray();
      // dd($seat);
      $message = "";
      $seat = DB::table('seats')
      ->leftJoin("customers",'customers.cus_id','=','seats.cus_id')
      ->get()->toArray();
      $customer = customer::all()->toArray();
      // dd($customer);
      return view('staffdirectory.shome',compact('seat','customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $book = seat::where('seat_id', $id)->first()->toArray();
      // dd($book);
      return view('staffdirectory.sbook',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->get("cancel"));
        $cancel = $request->get("cancel");
        // echo "<br>";
        // echo "ID User ".Session::get('user_id');
        $seat = seat::where('seat_id', $id)->first();

        // dd($seat);
        $cus_id = 0;
        if($cancel == null){
          $cus_id = $request->get('cus_id');
          $seat->cus_id = $cus_id;
        }
        else {
          // echo "".$cancel;
          $cus_id = $cancel;
          $seat->cus_id = $cus_id;
        }
        // dd($seat);
        $seat->save();
        //
        // $history = new history([
        //   'cus_id' => $cus_id,
        //   'seat_id' => $id
        // ]);
        // $history->save();

        return redirect()->route('sbooking.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchCustomer($id)
    {
        echo "searchCustomer";
    }
}
