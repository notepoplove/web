<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\customer;
class SBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "index";
        // $customer = customer::where('email', Input::get('email'))
        // ->orWhere('name', 'like', '%' . Input::get('name') . '%')->get();
        // dd();
        return view('staffdirectory.sbook');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // echo "create";
        return view('staffdirectory.sbook');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "store";
        $seat_id = $request->get('seat_id');
        $message = $request->get('message');
        if($seat_id != null){
          Session::put('seat_id',$seat_id);
        }
        $customer = customer::where('cus_name',$message)
        ->orWhere('cus_name', 'like', '%' . $message . '%')->get()->toArray();
        // dd($customer);
        $seat_id = Session::get('seat_id');
        return view('staffdirectory.sbook',compact('customer','message','seat_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // echo "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // echo "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo "update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
