<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\seat;
use DB;
class SSeatController extends Controller
{
    function manageSeat($cus_id){
      // $order = seat::find(DB::table('seats')->max('seat_id'))->toArray();
      // echo "".$order['seat_id'];
      $seat = new seat([
        'cus_id' => $cus_id
      ]);
      // dd($seat->toArray());
      $seat->save();
      return redirect()->route('sbooking.index');
    }
    function manageDelete(){
      // $seat = seat::all()->toArray();
      $order = seat::find(DB::table('seats')->max('seat_id'))->toArray();
      // dd($order);
      // count($array);
      // return redirect()->route('sbooking.index');
      DB::table('seats')->where('seat_id', '=', $order['seat_id'])->delete();
      return redirect()->route('sbooking.index');
    }
}
