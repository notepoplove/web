<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('index','UserController@showUser');
Route::get('slogin','UserController@showSLogin');
Route::get('/logout','UserController@checkLogout');
Route::get('/slogout','UserController@showSLogin');
Route::get('/searchCustomer/{message?}','SSearchController@searchCustomer');
Route::get('/manageSeat/{id?}','SSeatController@manageSeat');
Route::get('/deleteUser/{id?}','UserController@deleteUser');
Route::get('/manageDelete','SSeatController@manageDelete');
Route::post('/checklogin','UserController@checkLogin');
Route::get('/scheckLogin','UserController@scheckLogin');
// Route::get('booking','HomeController@showBooking');
Route::resource('userregister','RegisterController');
Route::resource('comment','CommentController');
Route::resource('booking','SeatController');
Route::resource('sbooking','SHomeController');
Route::resource('sbook','SBookController');

// Route::resource('bookingdestroy','SeatController@destroy');
// Route::resource('bookingindex','SeatController@index');
// Route::resource('bookingcreate','SeatController@create');
// Route::resource('bookingstore','SeatController@store');
// Route::resource('bookingshow','SeatController@show');
// Route::resource('bookingedit','SeatController@edit');
// Route::get('/fgf','SeatController@update');
