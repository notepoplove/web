<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/homeDefault.css')}}">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="alert/dist/sweetalert.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title></title>
  </head>
  <body>
    <!-- <div class="header" id="header">
      header
    </div> -->
    <div class="w3-sidebar w3-bar-block w3-black w3-card" id="sider"style="width:130px">

       <button class="w3-bar-item w3-button tablink" action="{{url('booking')}}" onclick="openLink(event, 'booking')">Booking</button>
       <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'history')">History</button>
       <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'comment')">Comment</button>
       <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'about')">About</button>

       <!-- <button class="w3-bar-item w3-button tablink" id="bt-logout" onclick="openLink(event, 'logout')">Logout</button> -->
       <a class="w3-bar-item w3-button tablink" id="bt-account" onclick="openLink(event, 'account')">account</a>
       <a href="/logout" class="w3-bar-item w3-button tablink" id="bt-logout">logout</a>
    </div>

    <div style="margin-left:130px">
       <div id="booking" class="w3-container city w3-animate-left" style="display:none">
         <table>
           <?php
              $i = 1;
              $number = 1;
           ?>
           <!-- @if(isset($seat)) -->
             @foreach($seat as $row)
                  <?php
                    if($i==1||$i==13){
                      echo '<tr>';
                      $i = 1;
                    }
                  ?>
                  <td>
                    <!-- <p>({{session('user_id')}})</p> -->
                    @if($row['cus_id']== 0)
                      <button class="pc" href="" onclick="reservationPC('.($number).')">PC <?php echo $row['seat_id']; ?>
                        <img class="computer" id="'.($row['seat_id']).'" src="image/computer.png" alt="HTML tutorial" style="width:80px;height:80px;">
                        <a href="{{action('SeatController@edit',$row['seat_id'])}}" class="btn btn-primary">book</a>
                      </button>
                    @elseif($row['cus_id'] == session('user_id'))
                      <button class="unusable" href="" onclick="reservationPC('.($row['seat_id']).')">PC <?php echo $number; ?>
                        <img class="computer" id="'.($row['seat_id']).'" src="image/computer.png" alt="HTML tutorial" style="width:80px;height:80px;">
                        <!-- <a href="{{action('SeatController@edit',0)}}" class="btn btn-primary">cance</a> -->
                        <form class="" action = "{{action('SeatController@update',$row['seat_id'])}}" method="post">
                          @csrf
                          {{method_field('PUT')}}
                          <input type="hidden" name="cancel" value="0">
                          <input type="submit" name="success" value="cancel" class="btn btn-primary" style="width:auto;height:auto;">
                        </form>
                      </button>
                    @else
                    <button class="unusable" href="" onclick="reservationPC('.($row['seat_id']).')">PC <?php echo $number; ?>
                      <img class="computer" id="'.($row['seat_id']).'" src="image/computer.png" alt="HTML tutorial" style="width:80px;height:80px;">
                      <a href="#" class="btn btn-primary btn-danger">X</a>
                    </button>
                    @endif
                  </td>
                <?php $i++; $number++; ?>
             @endforeach
           <!-- @endif -->

         </table>
       </div>

       <div id="home" class="w3-container div-home w3-animate-left" style="">
         <h2>home</h2>
         <p>Paris is the capital of France.</p>
         <p>The Paris area is one of the largest population centers in Europe, with more than 12 million inhabitants.</p>
       </div>

       <div id="history" class="w3-container city w3-animate-left" style="display:none">
         <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">PC</th>
              <th scope="col">Using</th>
              <th scope="col">Time</th>
            </tr>
          </thead>
          <tbody>
            <?php $number = 1;?>
              @if(isset($history))
                @foreach($history as $row)
                  <tr>
                    <th scope="row">{{$number}}</th>
                    <td>{{$row['seat_id']}}</td>
                    @if($row['cus_id']==0)
                      <td>in</td>
                    @else
                      <td>out</td>
                    @endif
                    <td>{{$row['created_at']}}</td>
                  </tr>
                  <?php $number++;?>
                @endforeach
              @endif

          </tbody>
        </table>
       </div>
       <div id="comment" class="w3-container city w3-animate-left" style="display:none">
         <div class="">
           <form class="" action="{{url('comment')}}" method="post">
             @csrf
             <table style="width:50%;height:auto;margin-top:15px;">
               <input type="hidden" name="cus_id" value="{{session('user_id')}}">
               <tr>
                <td>
                  <input type="text" name="message" placeholder="message" value="" style="width:100%;height:auto;">
                </td>
                <td>
                  <input type="submit" class="btn btn-primary" name="" value="send" style="width:auto;;height:auto;padding: 12px 20px;">
                </td>
               </tr>
             </table>
           </form>
             @if(isset($comment))
               @foreach($comment as $row)
                 @if($row->staff_id != 0)
                  <p class="comment">[Admin]: {{$row->com_message}}</p>
                 @else
                  <p class="comment">[{{$row->cus_name}}]: {{$row->com_message}}</p>
                 @endif
               @endforeach
             @endif
         </div>
       </div>

       <div id="about" class="w3-container city w3-animate-left" style="display:none">
         <h2>About</h2>

         <?php
            $pieces = explode("-", $store[key($store)]['store_detail']);
            for($i=1;$i<count($pieces);$i++){
              echo "<p>-".$pieces[$i]."</p>";
            }
          ?>
       </div>

       <div id="account" class="w3-container city w3-animate-left" style="display:none">
         <div class="container">
           @if(isset($customer))
           <form class="" action="{{action('RegisterController@update',$customer[key($customer)]['cus_id'])}}" method="post">
             @csrf
             {{method_field('PUT')}}
             <h1>Edit</h1>
             <p>Please fill in this form to edit an account.</p>
             <hr>
             <label for="name">Name</label>
             <input type="text" placeholder="name" name="name" value="{{$customer[key($customer)]['cus_name']}}" required>
             <label for="name">Current Password</label>
             <input type="password" placeholder="Password" name="psw" required>
             <label for="name">New Password</label>
             <input type="password" placeholder="New Password" name="npsw" required>
             <label for="name">Repeat Password</label>
             <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
             <label for="name">Phone number</label>
             <input type="text" placeholder="phone" name="phone" value="{{$customer[key($customer)]['cus_phone']}}" required>
             <label for="name">Address</label>
             <input type="text" placeholder="address" name="address" value="{{$customer[key($customer)]['cus_address']}}" required>
             <button type="submit" class="bt-form-login button-circle" style="width:100%;padding: 12px 20px;">Edit</button>
           </form>
           @endif
         </div>
       </div>

       <div id="logout" class="w3-container city w3-animate-left div-logout" style="display:none">
         <h2>Logout</h2>
         <p>Paris is the capital of France.</p>
         <p>The Paris area is one of the largest population centers in Europe, with more than 12 million inhabitants.</p>
       </div>
    </div>


    <script type="text/javascript">
      function openLink(evt, animName) {
        var i, x, tablinks;
        x = document.getElementsByClassName("city");

        var home = document.getElementsByClassName("div-home");
        home[0].style.display = "none"

        for (i = 0; i < x.length; i++) {
           x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
           tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
        }

        document.getElementById(animName).style.display = "block";
        if(animName != 'logout'){
          evt.currentTarget.className += " w3-red";
        }
      }
      function reservationPC(pc_id) {

        var text;

        // var str1 = "PC ";
        // var str2 = pc_id;
        // var message = str1.concat(str2);
        document.getElementById("pc-number").innerHTML = pc_id;
        // var price = prompt("Please enter your price", "");
        // if (price != "") {
        //
        //   window.location.href = "{{URL::to('home')}}"

          // var formData = new FormData();
          // prompt(price, price);
          // formData.append("pc_id", "pc_id");
          // formData.append("price", price); // number 123456 is immediately converted to a string "123456"
          //
          // // HTML file input, chosen by user
          // formData.append("userfile", fileInputElement.files[0]);
          //
          // // JavaScript file-like object
          // var content = '<a id="a"><b id="b">hey!</b></a>'; // the body of the new file...
          // var blob = new Blob([content], { type: "text/xml"});
          //
          // formData.append("webmasterfile", blob);
          //
          // // var request = new XMLHttpRequest();
          // request.open("POST", "https://www.youtube.com/watch?v=Pk7viRZXOAw&list=RDf3m5Jbqs11E&index=20");
          // request.send(formData);
          // document.getElementById("demo").innerHTML =
          // "Hello " + person + "! How are you today?";

        // }
      /*  var favDrink = prompt(res, pc_id);
        // openLink('event', 'booking');
        // switch(favDrink) {
        //   case "Martini":
        //     text = "Excellent choice. Martini is good for your soul.";
        //     break;
        //   case "Daiquiri":
        //     text = "Daiquiri is my favorite too!";
        //     break;
        //   case "Cosmopolitan":
        //     text = "Really? Are you sure the Cosmopolitan is your favorite?";
        //     break;
        //   default:
        //     text = "I have never heard of that one..";
        // }
        document.getElementById("demo").innerHTML = text;*/

        /*swal(message, {
          content: "input",
          buttons: {
            catch: {
              text: "Reservation",
              value: "catch",
            },
            defeat: "Cancel",
            cancel: "Close",
          },
        })
        .then((value) => {
          switch (value) {
            case "defeat":
              swal("Cancel reservation.");
               // window.open("home"); // open link
              break;

            case "catch":
              // const name = movie.trackName;

              swal(name, "Reservation successful.", "success");
              break;

            // default:
            //   swal("Got away safely!");
          }
        });*/
      }
    </script>

  </body>
</html>
