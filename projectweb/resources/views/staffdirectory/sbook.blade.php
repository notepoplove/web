<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/bookDefault.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <title></title>
  </head>
  <body>
    <div class="container" style="margin-top:20px;">
      <form class="" action="{{url('sbook')}}" method="post">
        @csrf
        <table>
          <tr>
            <td style="width:100%">
                <input type="text" name="message" value="{{$message}}" style="width:100%" required>
            </td>
            <td>
              <input type="submit" name="" value="search">
            </td>
          </tr>
        </table>
      </form>
      <table class="table">
        @if(isset($customer))
          @foreach($customer as $row)
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td style="width:100%">{{$row['cus_name']}}</td>
                <td>
                  <form class="" action = "{{action('SHomeController@update',$seat_id)}}" method="post">
                    @csrf
                    {{method_field('PUT')}}
                    <input type="hidden" name="cus_id" value="{{$row['cus_id']}}">
                    <input type="submit" name="" class="btn btn-primary" value="book">
                  </form>
                </td>
              </tr>
            </tbody>
            @endforeach
        @endif
      </table>
    </div>
  </body>
</html>
