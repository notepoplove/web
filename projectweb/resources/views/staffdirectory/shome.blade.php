<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/homeDefault.css')}}">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="alert/dist/sweetalert.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title></title>
  </head>
  <body>
    <!-- <div class="header" id="header">
      header
    </div> -->
    <div class="w3-sidebar w3-bar-block w3-black w3-card" id="sider"style="width:130px">

       <button class="w3-bar-item w3-button tablink" action="{{url('booking')}}" onclick="openLink(event, 'booking')">Booking</button>
       <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'history')">History</button>
       <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'comment')">Comment</button>
       <button class="w3-bar-item w3-button tablink" onclick="openLink(event, 'about')">About</button>

       <!-- <button class="w3-bar-item w3-button tablink" id="bt-logout" onclick="openLink(event, 'logout')">Logout</button> -->
       <a class="w3-bar-item w3-button tablink" id="bt-account" onclick="openLink(event, 'account')">account</a>
       <a href="/slogout" class="w3-bar-item w3-button tablink" id="bt-logout">logout</a>
    </div>

    <div style="margin-left:130px">
       <div id="booking" class="w3-container city w3-animate-left" style="display:none">
         <!-- <p>{{count($seat)}}</p> -->
         <a href="/manageSeat/0" class="btn btn-primary">add</a>
         <a href="/manageDelete" class="btn btn-primary btn-danger">delete</a>
         @if(isset($seat))
         <table class="table">
           <thead>
            <tr>
              <th scope="col">PC</th>
              <th scope="col">User</th>
              <th scope="col">Manage</th>
            </tr>
          </thead>
           @foreach($seat as $row)
           <tbody>
             <tr>
               <th scope="row">{{$row->seat_id}}</th>
               <td>{{$row->cus_name}}</td>
               <td>
                 <table>
                   <tr>
                     @if($row->cus_id==0)
                       <td>
                         <form class="" action = "{{url('sbook')}}" method="post">
                           @csrf
                           <input type="hidden" name="seat_id" value="{{$row->seat_id}}">
                           <input type="submit" name="success" value="book" class="btn btn-primary" style="width:80px;height:auto;">
                         </form>
                       </td>
                     @else
                       <td>
                         <form class="" action = "{{action('SHomeController@update',$row->seat_id)}}" method="post">
                           @csrf
                           {{method_field('PUT')}}
                           <input type="hidden" name="cancel" value="0">
                           <input type="submit" name="success" value="cancel" class="btn btn-primary btn-danger" style="width:80px;height:auto;">
                         </form>
                       </td>
                     @endif
                   </tr>
                 </table>
               </td>
             </tr>
           </tbody>
           @endforeach
         </table>
         @endif
       </div>

       <div id="home" class="w3-container div-home w3-animate-left" style="">
         <h2>home</h2>
         <p>Paris is the capital of France.</p>
         <p>The Paris area is one of the largest population centers in Europe, with more than 12 million inhabitants.</p>
       </div>

       <div id="history" class="w3-container city w3-animate-left" style="display:none">
         <h2>history</h2>
       </div>
       <div id="comment" class="w3-container city w3-animate-left" style="display:none">
         <h2>comment</h2>
       </div>

       <div id="about" class="w3-container city w3-animate-left" style="display:none">
         <h2>About</h2>
         <p>Paris is the capital of France.</p>
         <p>The Paris area is one of the largest population centers in Europe, with more than 12 million inhabitants.</p>
       </div>

       <div id="account" class="w3-container city w3-animate-left" style="display:none">
         <h2>account</h2>
         @if(isset($customer))
         <table class="table">
           <thead class="thead-dark">
             <tr>
               <th scope="col">#</th>
               <th scope="col">Name</th>
               <th scope="col">Email</th>
               <th scope="col">Delete</th>
             </tr>
           </thead>
          <?php $i=1; ?>
          @foreach($customer as $row)
            <tbody>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td>{{$row['cus_name']}}</td>
                <td>{{$row['cus_username']}}</td>
                <td>
                  <a href="/deleteUser/{{$row['cus_id']}}" class="btn btn-primary btn-danger">delete</a>
                </td>
              </tr>
            </tbody>
            <?php $i++; ?>
          @endforeach
          </table>
         @endif
       </div>

       <div id="logout" class="w3-container city w3-animate-left div-logout" style="display:none">
         <h2>Logout</h2>
         <p>Paris is the capital of France.</p>
         <p>The Paris area is one of the largest population centers in Europe, with more than 12 million inhabitants.</p>
       </div>
    </div>


    <script type="text/javascript">
      function openLink(evt, animName) {
        var i, x, tablinks;
        x = document.getElementsByClassName("city");

        var home = document.getElementsByClassName("div-home");
        home[0].style.display = "none"

        for (i = 0; i < x.length; i++) {
           x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
           tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
        }

        document.getElementById(animName).style.display = "block";
        if(animName != 'logout'){
          evt.currentTarget.className += " w3-red";
        }
      }
      function reservationPC(pc_id) {

        var text;

        // var str1 = "PC ";
        // var str2 = pc_id;
        // var message = str1.concat(str2);
        document.getElementById("pc-number").innerHTML = pc_id;
        // var price = prompt("Please enter your price", "");
        // if (price != "") {
        //
        //   window.location.href = "{{URL::to('home')}}"

          // var formData = new FormData();
          // prompt(price, price);
          // formData.append("pc_id", "pc_id");
          // formData.append("price", price); // number 123456 is immediately converted to a string "123456"
          //
          // // HTML file input, chosen by user
          // formData.append("userfile", fileInputElement.files[0]);
          //
          // // JavaScript file-like object
          // var content = '<a id="a"><b id="b">hey!</b></a>'; // the body of the new file...
          // var blob = new Blob([content], { type: "text/xml"});
          //
          // formData.append("webmasterfile", blob);
          //
          // // var request = new XMLHttpRequest();
          // request.open("POST", "https://www.youtube.com/watch?v=Pk7viRZXOAw&list=RDf3m5Jbqs11E&index=20");
          // request.send(formData);
          // document.getElementById("demo").innerHTML =
          // "Hello " + person + "! How are you today?";

        // }
      /*  var favDrink = prompt(res, pc_id);
        // openLink('event', 'booking');
        // switch(favDrink) {
        //   case "Martini":
        //     text = "Excellent choice. Martini is good for your soul.";
        //     break;
        //   case "Daiquiri":
        //     text = "Daiquiri is my favorite too!";
        //     break;
        //   case "Cosmopolitan":
        //     text = "Really? Are you sure the Cosmopolitan is your favorite?";
        //     break;
        //   default:
        //     text = "I have never heard of that one..";
        // }
        document.getElementById("demo").innerHTML = text;*/

        /*swal(message, {
          content: "input",
          buttons: {
            catch: {
              text: "Reservation",
              value: "catch",
            },
            defeat: "Cancel",
            cancel: "Close",
          },
        })
        .then((value) => {
          switch (value) {
            case "defeat":
              swal("Cancel reservation.");
               // window.open("home"); // open link
              break;

            case "catch":
              // const name = movie.trackName;

              swal(name, "Reservation successful.", "success");
              break;

            // default:
            //   swal("Got away safely!");
          }
        });*/
      }
    </script>

  </body>
</html>
