<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('css/userDefault.css')}}">

</head>
<body>
  <table>
    <tr>
      <td>
        <button class=""="bt-login" id="bt-login" onclick="document.getElementById('id01').style.display='block'" style="width:150px;height:50px;">Login</button>
      </td>
      <td>
        <button class=""="bt-register" id="bt-register" onclick="document.getElementById('id02').style.display='block'" style="width:150px;height:50px;">Register</button>
      </td>
    </tr>
  </table>
  <div id="id01" class="modal">
    <form method="post" class="modal-content animate" action="/checklogin">
      @csrf
      <div class="container">
        <input type="text" placeholder="Enter Username" name="username" required>
        <input type="password" placeholder="Enter Password" name="password" required>
        <button class="bt-form-login" type="submit">Login</button>
        <label>
          <input type="checkbox" checked="checked" name="remember"> Remember me
        </label>
      </div>

      <div class="container">
        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
      </div>
    </form>
  </div>

  <div id="id02" class="modal">
    <!-- <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span> -->
    <form method="post" class="modal-content animate" action="{{url('userregister')}}">
      @csrf
      <div class="container">
        <h1>Sign Up</h1>
        <p>Please fill in this form to create an account.</p>
        <hr>
        <input type="text" placeholder="name" name="name" required>
        <input type="text" placeholder="Enter Email" name="email" required>
        <input type="password" placeholder="Enter Password" name="psw" required>
        <input type="password" placeholder="Repeat Password" name="psw-repeat" required>

            <div class="nativeDatePicker">
              <input type="date" id="bday" name="bday">
              <span class="validity"></span>
            </div>
            <p class="fallbackLabel" style="display: none;">Enter your birthday:</p>
            <div class="fallbackDatePicker" style="display: none;">
              <span>
                <label for="day">Day:</label>
                <select id="day" name="day">
                </select>
              </span>
              <span>
                <label for="month">Month:</label>
                <select id="month" name="month">
                  <option selected="">January</option>
                  <option>February</option>
                  <option>March</option>
                  <option>April</option>
                  <option>May</option>
                  <option>June</option>
                  <option>July</option>
                  <option>August</option>
                  <option>September</option>
                  <option>October</option>
                  <option>November</option>
                  <option>December</option>
                </select>
              </span>
              <span>
                <label for="year">Year:</label>
                <select id="year" name="year">
                </select>
              </span>
            </div>

        <input type="text" placeholder="phone" name="phone" required>
        <input type="text" placeholder="address" name="address" required>

        <label>
          <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
        </label>
        <button type="submit" class="bt-form-login">Sign Up</button>
        <div class="clearfix">
          <button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
      </div>
    </form>
  </div>


  <script>
    // define variables
    var nativePicker = document.querySelector('.nativeDatePicker');
    var fallbackPicker = document.querySelector('.fallbackDatePicker');
    var fallbackLabel = document.querySelector('.fallbackLabel');

    var yearSelect = document.querySelector('#year');
    var monthSelect = document.querySelector('#month');
    var daySelect = document.querySelector('#day');

    // hide fallback initially
    fallbackPicker.style.display = 'none';
    fallbackLabel.style.display = 'none';

    // test whether a new date input falls back to a text input or not
    var test = document.createElement('input');
    test.type = 'date';

    // if it does, run the code inside the if() {} block
    if(test.type === 'text') {
      // hide the native picker and show the fallback
      nativePicker.style.display = 'none';
      fallbackPicker.style.display = 'block';
      fallbackLabel.style.display = 'block';

      // populate the days and years dynamically
      // (the months are always the same, therefore hardcoded)
      populateDays(monthSelect.value);
      populateYears();
    }

    function populateDays(month) {
      // delete the current set of <option> elements out of the
      // day <select>, ready for the next set to be injected
      while(daySelect.firstChild){
        daySelect.removeChild(daySelect.firstChild);
      }

      // Create variable to hold new number of days to inject
      var dayNum;

      // 31 or 30 days?
      if(month === 'January' || month === 'March' || month === 'May' || month === 'July' || month === 'August' || month === 'October' || month === 'December') {
        dayNum = 31;
      } else if(month === 'April' || month === 'June' || month === 'September' || month === 'November') {
        dayNum = 30;
      } else {
      // If month is February, calculate whether it is a leap year or not
        var year = yearSelect.value;
        var leap = (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
        dayNum = leap ? 29 : 28;
      }

      // inject the right number of new <option> elements into the day <select>
      for(i = 1; i <= dayNum; i++) {
        var option = document.createElement('option');
        option.textContent = i;
        daySelect.appendChild(option);
      }

      // if previous day has already been set, set daySelect's value
      // to that day, to avoid the day jumping back to 1 when you
      // change the year
      if(previousDay) {
        daySelect.value = previousDay;

        // If the previous day was set to a high number, say 31, and then
        // you chose a month with less total days in it (e.g. February),
        // this part of the code ensures that the highest day available
        // is selected, rather than showing a blank daySelect
        if(daySelect.value === "") {
          daySelect.value = previousDay - 1;
        }

        if(daySelect.value === "") {
          daySelect.value = previousDay - 2;
        }

        if(daySelect.value === "") {
          daySelect.value = previousDay - 3;
        }
      }
    }

    function populateYears() {
      // get this year as a number
      var date = new Date();
      var year = date.getFullYear();

      // Make this year, and the 100 years before it available in the year <select>
      for(var i = 0; i <= 100; i++) {
        var option = document.createElement('option');
        option.textContent = year-i;
        yearSelect.appendChild(option);
      }
    }

    // when the month or year <select> values are changed, rerun populateDays()
    // in case the change affected the number of available days
    yearSelect.onchange = function() {
      populateDays(monthSelect.value);
    }

    monthSelect.onchange = function() {
      populateDays(monthSelect.value);
    }

    //preserve day selection
    var previousDay;

    // update what day has been set to previously
    // see end of populateDays() for usage
    daySelect.onchange = function() {
      previousDay = daySelect.value;
    }
  </script>

  <script>
  // Get the modal
  var modal1 = document.getElementById('id01');
  var modal2 = document.getElementById('id02');
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal1) {
      modal.style.display = "none";
    }
    else if (event.target == modal2) {
      modal.style.display = "none";
    }
  }
  </script>

</body>
</html>
